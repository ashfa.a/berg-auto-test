const express = require('express');
const bodyParser = require("body-parser");
//require('dotenv').config()
const process = require('./process')
const app = express()

app.use(bodyParser.json());

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "OPTIONS, GET, POST, PUT, PATCH, DELETE"
  );
  res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
  next();
});

app.use('/', process);

app.listen( 3000 , () => {
  console.log('app is listening')
})
