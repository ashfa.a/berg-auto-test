# from PIL import Image, ImageChops

# img1 = Image.open('imggg1.png')
# img2 = Image.open('imggg2.png')

# diff = ImageChops.difference(img1,img2)

# # if diff.getbbox():
# #     diff.show()
# print(diff.getbbox())
# importer ceci

from skimage.measure import compare_ssim
import imutils
import cv2
import os
import fileinput
import sys

# files = fileinput.input()
# for line in files:
#     if fileinput.isfirstline():
#         print(f'\n--- Reading {fileinput.filename()} ---')
#     print(' -> ' + line, end='')
# print()

def load_images_from_folder(folder):
    images = []
    for filename in os.listdir(folder):
        img = cv2.imread(os.path.join(folder,filename))
        if img is not None:
            images.append(img)
    return images

# construction
# imageA=open()

# images
imageA = cv2.imread("/home/sajithan/Desktop/Projects/differences-between-two-images/dataset/image-1.jpg")
imageB = cv2.imread("/home/sajithan/Desktop/Projects/differences-between-two-images/dataset/image-2.jpg")
dim = (500, 500)

imageA = cv2.resize(imageA, dim, interpolation = cv2.INTER_AREA);
imageB = cv2.resize(imageB, dim, interpolation = cv2.INTER_AREA)
# convertion en gris
grayA = cv2.cvtColor(imageA, cv2.COLOR_BGR2GRAY)
grayB = cv2.cvtColor(imageB, cv2.COLOR_BGR2GRAY)

# Calcule de 'Structural Similarity '(SSIM) 
# images, assure le retour
(score, diff) = compare_ssim(grayA, grayB, full=True)
diff = (diff * 255).astype("uint8")
print("indice de simularité SSIM: {}".format(score))

# detection de contour
# obtain the regions of the two input images that differ
thresh = cv2.threshold(diff, 0, 255,
	cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
	cv2.CHAIN_APPROX_SIMPLE)
cnts = imutils.grab_contours(cnts)

# loop over the contours
for c in cnts:
	#dessiner un rectangle rouge autour
	(x, y, w, h) = cv2.boundingRect(c)
	cv2.rectangle(imageA, (x, y), (x + w, y + h), (0, 0, 255), 2)
	cv2.rectangle(imageB, (x, y), (x + w, y + h), (0, 0, 255), 2)

# afficher les images
cv2.imshow("Original", image)
cv2.imshow("Modified", imageB)
cv2.imshow("Diff", dif)
cv2.imshow("Thresh", thresh)
cv2.waitKey(0)
