let GL = require("./modules/gitlab-module.js");
const myConfig = require("./stylelint/stylelintrc.json");
var fs = require("fs");
const express = require("express");
const stylelint = require("stylelint");
const { ESLint } = require("eslint");
const { exec } = require("child_process");
const { Script } = require("vm");
const router = express.Router();
let tree = new Array();
tree = [];
let results = [];

router.post("/", async (req, res) => {
  const url = req.body.url;
  const token = req.body.token;
  const subPath = req.body.subPath;

  //Get gitlab project id and file tree
  const id = await GL.getGitProjectId(url, token);
  tree = await GL.getFileTree(id, token, subPath);
  await readFiles(id, token);

  let rs = {
    results: results,
  };
  res.contentType("application/json");
  await res.end(JSON.stringify(rs));
  results = [];
  tree = [];
});

async function readFiles(id, token) {
  let data = new Array();
  data = [];
  for (var i = 0; i < tree.length; i++) {
    data.push(await GL.getFileContent(id, token, tree[i].path));
    let resData = await checkCodeQuality(data[i]);
    if (resData != null) results.push(resData);
  }
  return data;
}

async function checkCodeQuality(data) {
  let res = [];
  if (data[0].fileName != undefined) {
    //find file type
    let fname = data[0].fileName;
    let ext = fname.split(".");

    //for css files and scss files
    if (ext[1] == "css" || ext[1] == "scss") {
      await stylelint
        .lint({
          config: myConfig,
          code: data[0].content,
        })
        .then(function (d) {
          let setWarning = [
            {
              line: JSON.parse(d.output)[0].warnings[0].line,
              column: JSON.parse(d.output)[0].warnings[0].column,
              message: JSON.parse(d.output)[0].warnings[0].text,
            },
          ];
          res = [
            {
              file: data[0].fileName,
              path: data[0].filePath,
              warnings: setWarning,
            },
          ];
        })
        .catch(function (err) {
          //console.error(err.msg);
        });
    }
    if (ext[1] == "js") {
      const eslint = new ESLint();
      const results = await eslint.lintText(data[0].content);
      const formatter = await eslint.loadFormatter("json");
      const resultText = formatter.format(results);
      let def = JSON.parse(resultText)[0].messages[0];
      if (def != undefined) {
        let warningData = [
          {
            line: JSON.parse(resultText)[0].messages[0].line,
            column: JSON.parse(resultText)[0].messages[0].column,
            message: JSON.parse(resultText)[0].messages[0].message,
          },
        ];
        res = [
          {
            file: data[0].fileName,
            path: data[0].filePath,
            warnings: warningData,
          },
        ];
      }
    } else if (ext[1] == "php") {
      let phpErrors = new Array();
      await fs.writeFileSync("mytext.php", data[0].content);
      await exec(
        "vendor/bin/phpcs --standard=WordPress mytext.php",
        (error, stdout, stderr) => {
          phpErrors.push(JSON.parse(stdout)[0].messages);
        }
      );
      //fs.unlinkSync("")S
      res = [
        {
          file: data[0].fileName,
          path: data[0].filePath,
          warnings: phpErrors,
        },
      ];
    } else if (ext[1] == "py") {
      let pyErrors = new Array();
      let arr = new Array();
      await fs.writeFileSync("script.py", data[0].content);

      await exec("pylint script.py", (error, stdout, stderr) => {
        let lines = stdout.split("script.py");
        let resultDetailes = new Array();
        for (var i = 1; i < lines.length - 2; i++) {
          let results = lines[i].split(":");
          resultD = [
            {
              line: results[1],
              column: results[2],
              message: results[4],
            },
          ];
          resultDetailes.push(resultD);
        }
        pyErrors.push(resultDetailes);
      });

      res = [
        {
          file: data[0].fileName,
          path: data[0].filePath,
          warnings: pyErrors,
        },
      ];
    }
  }
  return res;
}

module.exports = router;
