var axios = require("axios");
var fs = require("fs");
const { response } = require("express");
const express = require("express");
const APIURL = "https://gitlab.com/api/v4";

//get gitlab project id
async function getGitProjectId(repoUrl, token) {
  let id;
  var url = repoUrl.split("/");
  var config = {
    method: "get",
    url:
      APIURL + "/projects/" + url[url.length - 2] + "%2F" + url[url.length - 1],
    headers: {
      Authorization: "Bearer " + token,
    },
  };
  await axios(config)
    .then(function (response) {
      id = response.data.id;
    })
    .catch(function (error) {
      console.log(error);
    });

  if (id) return id;
  else return "Error occurs in input values";
}

//list project files tree
async function getFileTree(id, token, subPath) {
  let tree = new Array();
  let isNotLastPage = true;
  let i = 1;
  let epath = "";

  while (isNotLastPage) {
    if (subPath) {
      epath = await encodePath(subPath);
      var config = {
        method: "get",
        url:
          APIURL +
          "/projects/" +
          id +
          "/repository/tree?per_page=100&recursive=true&page=" +
          i +
          "&path=" +
          epath,
        headers: {
          Authorization: "Bearer " + token,
        },
      };
    } else {
      var config = {
        method: "get",
        url:
          APIURL +
          "/projects/" +
          id +
          "/repository/tree?per_page=100&recursive=true&page=" +
          i,
        headers: {
          Authorization: "Bearer " + token,
        },
      };
    }

    await axios(config)
      .then(function (response) {
        tree.push(...response.data);
      })
      .catch(function (error) {
        console.log(error);
      });

    if (tree.length % 100 != 0) {
      isNotLastPage = false;
    }
    i++;
  }
  if (tree) return tree;
  else return "Error occurs in input values";
}

//get file content
async function getFileContent(id, token, path) {
  let content, file_data;
  let epath = "";
  let isFile = path.split(".");

  //check for subpath
  let isSubPath = path.split("/");
  let config;
  console.log("isfile", isFile);
  console.log("isSub", isSubPath);
  if (isFile.length == 2) {
    //subpaths are available
    if (isSubPath.length >= 2) {
      //encode if there's any subpaths

      epath = await encodePath(path);
      console.log("epath", epath);
      config = {
        method: "get",
        url:
          APIURL +
          "/projects/" +
          id +
          "/repository/files/" +
          epath +
          "?ref=master",
        headers: {
          Authorization: "Bearer " + token,
        },
      };
    } else {
      console.log("path", path);
      //don't have subpaths
      config = {
        method: "get",
        url:
          APIURL +
          "/projects/" +
          id +
          "/repository/files/" +
          path +
          "?ref=master",
        headers: {
          Authorization: "Bearer " + token,
        },
      };
    }
    await axios(config)
      .then(function (response) {
        data = response.data;
        content = response.data.content;
      })
      .catch(function (error) {
        console.log(error);
      });
    if (content) {
      let buff = Buffer.from(content, "base64");
      let text = buff.toString("ascii");
      let res = [
        {
          fileName: data.file_name,
          filePath: data.file_path,
          size: data.size,
          content: text,
        },
      ];
      return res;
    } else {
    }
    return "Empty file";
  } else if (isFile.length == 3) {
    return "Hidden file";
  } else {
    return "This is a directory";
  }
}

//convert path to encoded path
async function encodePath(path) {
  let epath = "";
  epath = path.replace(".", "%2E");
  epath = epath.split("/").join("%2F");
  return epath;
}

// Exporting variables and functions
module.exports = {
  getGitProjectId,
  getFileTree,
  getFileContent,
  encodePath,
};
